# Formato de Tesis Postgrado UdeC

Está diseñado para el uso de LaTeX. Satisface los lineamientos dados en [por la SIBUDEC](http://www.bibliotecas.udec.cl/sites/default/files/PAUTAS_DE_NORMALIZACION_EN_LA_PRESENTACION_DE_UNA_TESIS_DE_GRADO_O_TITULACION_Mayo.pdf).

Este formato de tesis está diseñado para los programas de postgrado en Ciencias Físicas de la Universidad de Concepción, en partícular para el programa de Magíster en Ciencias con Mención en Física. Sin embargo, es fácilmente adaptable al programa del usuario. 

La licencia "[The Unlicense](https://unlicense.org)" permite la utilización del presente formato sin necesidad 
de publicar el código fuente del trabajo "derivado" y no incluye términos que obliguen al usuario a ocupar la misma licencia.

Falta el escudo de la UdeC en el directorio figuras para compilar. Una opción es descargar [éste archivo](http://www.udec.cl/normasgraficas/sites/default/files/escudo.png) o cualquiera de la [página de normas gráficas](http://www.udec.cl/normasgraficas/node/4) y guardarlo en el directorio figuras con el nombre `udec.png`. 

Cada uno de los archivos fuente lleva un *comentario mágico* de la forma
```LaTeX
% !TeX spellcheck = es_CL
% !TEX encoding = UTF-8 Unicode
```
que permiten a los editores (los compatibles al menos) de LaTeX identificar el idioma del texto y la codificación respectivamente.

## Estructura de los archivos
* `Tesis.tex` es el archivo maestro (el único que se debe compilar),
* el directorio `preliminares` contiene los capítulos y partes de la estructura necesaria para las páginas iniciales de la tesis,
* el directorio `capitulos` contiene el texto de la tesis,
* el directorio `bibliografía` contiene el archivo `.bib` con las referencias (formaton BibTeX) y
* `figuras` contiene a las figuras necesarias para la tesis.

## Requerimientos

En una instalación LaTeX básica en sistemas basados en Debian (Ubuntu/Mint/raspbian/etc...) se necesitan los siguientes paquetes extra aparte de `texlive` (basta instalarlos con `apt install paquete`):
* `texlive-lang-spanish`: si se va a escribir la tesis en español y se desea usar el paquete `babel`.
* `texlive-latex-extra`: para usar `multirow`, `emptypage`, `anyfontsize`, `csquotes` y `lipsum` (que puede eliminarse ya que genera texto sin sentido para rellenar); 
* `texlive-fonts-extra`: para usar las fuentes Times,
* `texlive-bibtex-extra` y `biber`: para manejo de la bibliografía.

Por favor dirigirse a la documentación de cada uno de éstos paquetes si necesita ayuda adicional.
